# Tack - Tattle's Front-end AngularJS Challenge

We are providing a basic websockets server [Socket.io](http://socket.io/) to act as a robust back-end for a front-end that has yet to be developed, by you!

The micro-application is not complete, by any means, and intentionally so.

When you are ready, set aside some uninterrupted time, and fork this repository and do the additional development required
to integrate with the socket.io backend and create a front-end chat that you think we'll like!

#### Required
- User needs to chat in real-time with another User.
- User must enter their username before chatting (you can assume all usernames are unique)
- User can switch to another channel
- User needs to be able to view all available Channels, while in any channel
- When a User changes to a different channel, the chat display needs to be cleared.
- This interface needs to be clean with good UI/UX and must make use of the following colors
    - primary:         ```#1CA6DF```
    - secondary:    ```#F78F1E```

#### Preferred
- User can see when another user is typing
- User does not receive their own messages, instead they are added to the local messages instantly
- When a User returns to a Channel where they were previously, the chat history from the current session appears (persistent data is a bonus)


Install package and start server:
```sh
npm install && node index.js
```
Include the Socket library in your HTML solution:
```html    
<script src="http://localhost:3000/socket.io/socket.io.js"></script>
```

# Socket events
### Emitted by server
```start``` - emitted when a connection is established
```js
{
    channels: [
        {label: String, private: Boolean}
    ],
    canViewPrivate: Boolean
}
```

```message_created``` - emitted when the server receives ```new_message```
```js
{
    user: {
        username: String
    },
    message: String,
    created: Date
}
```

### Emitted by server or client
You must use the socket client JS library to trigger (```emit```) and handle (```on```) the following events.

```user_joined``` - emitted when a user joins a channel
```js
{
    channel: {
        label: String,
        private: Boolean
    },
    user: {
        username: String
    }
}
```

```user_left``` - emitted when a user leaves a channel
```js
{
    channel: {
        label: String,
        private: Boolean
    },
    user: {
        username: String
    }
}
```

```change_channel``` - emitted when a user changes channels, leaving or and joining another
```js
{
    prevChannel: {
        label: String,
        private: Boolean
    },
    newChannel: {
        label: String,
        private: Boolean
    },
    user: {
        username: String
    }
}
```

```new_message``` - emitted when a user sends a message
```js
{
    channel: {
        label: String,
        private: Boolean
    },
    user: {
        username: String
    },
    message: String
}
```

```typing_event``` - indicates a user has either started or stopped typing
```js
{
    channel: {
        label: String,
        private: Boolean
    },
    user: {
        username: String
    },
    message: String
}
```
