var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var chalk = require('chalk');

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/public/index.html');
});

//Listen for connection
io.on('connection', function(socket) {
  console.log('connected');
  //Globals
  var defaultChannel = 'general';
  var channels = [
    { label: "general", private: false },
    { label: "tattle", private: true },
    { label: "developers", private: true },
    { label: "merchants", private: false },
    { label: "customers", private: false },
    { label: "rewards", private: false }
  ];

  //Emit Startup data
  socket.emit('start', {
    channels: channels,
    canViewPrivate: true
  });

  //When a user joins a channel
  socket.on('user_joined', function(data) {
    //Join default channel when joining
    socket.join(data.channel.label);
    //Tell all people in channel about new user
    io.in(data.channel.label).emit('user_joined', data);
    console.log(chalk.bold(data.user.username) + ' ' + chalk.green('joined') + ' ' + data.channel.label);
  });

  //When a user changes channels
  socket.on('change_channel', function(data) {
    socket.leave(data.prevChannel.label);
    io.in(data.prevChannel).emit('user_left', { channel: data.prevChannel, user: data.user });
    console.log(chalk.bold(data.user.username) + ' ' + chalk.red('left') + ' ' +  data.prevChannel.label);

    socket.join(data.newChannel.label);
    io.in(data.newChannel.label).emit('user_joined', { channel: data.newChannel, user: data.user });
    console.log(chalk.bold(data.user.username) + ' ' + chalk.green('joined') + ' ' + data.newChannel.label);
  });

  //When a user is typing
  socket.on('typing_event', function(data) {
    socket.broadcast.to(data.channel.label).emit('typing_event', data);
  });

  //On new message
  socket.on('new_message', function(data) {
    //Create message
    var newMsg = {
      user: data.user,
      message: data.message,
      created: new Date()
    };
    //Send message to those connected in the room
    socket.broadcast.to(data.channel.label).emit('message_created', newMsg);
    console.log(chalk.bold(data.user.username) + chalk.yellow(' sent a message'));
  });
});

http.listen(3000, function(){
  console.log(chalk.bold.blue('Tattle') + ' Front-End Chat Coding Challenge');
  console.log('server is listening on ' + chalk.yellow('*:3000'));
});
